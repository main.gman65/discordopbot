const fs = require('fs');
const yml = require("../yml.js");
module.exports = async (bot) => {
    let config;
    if(!fs.existsSync("./config/vcstatus.yml")) {
        if(!fs.existsSync("./config/")) {
            fs.mkdir('./config/', function() {
                fs.writeFile("./config/vcstatus.yml", "Status_Category_Name: 'Server Stats'\nBot_Count: 'true'\nMember_Count: 'true'\nTotal_Users_Count: 'true'\nChannel_Count: 'true'", async function(err) { 
                    if(err) throw err; 
                    config = await yml("./config/vcstatus.yml");
                })
            })
        }
    } else {
        config = await yml("./config/vcstatus.yml");
    }
    setTimeout(function() {
        async function update() {
            let channel = bot.channels.find(c => c.name.toLowerCase() == config.Status_Category_Name.toLowerCase() && c.type == 'category');
            if(!channel) return;
            let guild = channel.guild;
            let botCount = channel.children.find(c => c.name.match(/Bots: \d+/g));
            let memberCount = channel.children.find(c => c.name.match(/Members: \d+/g));
            let totalUserCount = channel.children.find(c => c.name.match(/Total Users: \d+/g));
            let channelCount = channel.children.find(c => c.name.match(/Channels: \d+/g));
            //Bot Count
            if(botCount && config.Bot_Count == 'true') {
                botCount.setName(`Bots: ${bot.users.filter(u => u.bot).size}`); 
            }
    
            //Member Count
            if(memberCount && config.Member_Count == 'true') {
                memberCount.setName(`Members: ${bot.users.filter(u => !u.bot).size}`); 
            }
            //Total User Count
            if(totalUserCount && config.Total_Users_Count == 'true') {
                totalUserCount.setName(`Total Users: ${bot.users.size}`); 
            }
            //Channel Count
            if(channelCount && config.Channel_Count == 'true') {
                channelCount.setName(`Channels: ${bot.channels.size}`); 
            }
    
            if(!botCount && config.Bot_Count == 'true') {
                let chnl = await guild.createChannel(`Bots: ${bot.users.filter(u => u.bot).size}`, 'voice');
                await chnl.setParent(channel.id);
                await chnl.overwritePermissions(guild.id, {"CONNECT": false, "SPEAK": false});
            }
    
            if(!memberCount && config.Member_Count == 'true') {
                let chnl = await guild.createChannel(`Members: ${bot.users.filter(u => !u.bot).size}`, 'voice');
                await chnl.setParent(channel.id);
                await chnl.overwritePermissions(guild.id, {"CONNECT": false, "SPEAK": false});
            }
    
            if(!totalUserCount && config.Total_Users_Count == 'true') {
                let chnl = await guild.createChannel(`Total Users: ${bot.users.size}`, 'voice');
                await chnl.setParent(channel.id);
                await chnl.overwritePermissions(guild.id, {"CONNECT": false, "SPEAK": false});
            }
    
            if(!channelCount && config.Channel_Count == 'true') {
                let chnl = await guild.createChannel(`Channels: ${bot.channels.size}`, 'voice');
                await chnl.setParent(channel.id);
                await chnl.overwritePermissions(guild.id, {"CONNECT": false, "SPEAK": false});
            }

            if(botCount && config.Bot_Count == 'false') {
                botCount.delete();
            }
            if(memberCount && config.Member_Count == 'false') {
                memberCount.delete();
            }
            if(totalUserCount && config.Total_Users_Count == 'false') {
                totalUserCount.delete();
            }
            if(channelCount && config.Channel_Count == 'false') {
                channelCount.delete();
            }
        }
        update();
        setInterval(update, 10000)
    }, 2500)
};