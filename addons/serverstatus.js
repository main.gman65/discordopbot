const fs = require('fs');
const yml = require("../yml.js");
const request = require("request-promise");
module.exports = async (bot) => {
    let config;
    if(!fs.existsSync("./config/serverstatus.yml")) {
        if(!fs.existsSync("./config/")) {
            fs.mkdir('./config/', function() {
                fs.writeFile("./config/serverstatus.yml", "Server_Status_Category_Name: 'Server Status'\nServer_IP: 'mc.hypixel.net'\nServer_Port: 25565", async function(err) { 
                    if(err) throw err; 
                    config = await yml("./config/serverstatus.yml");
                })
            })
        } else {
            fs.writeFile("./config/serverstatus.yml", "Server_Status_Category_Name: 'Server Status'\nServer_IP: 'mc.hypixel.net'\nServer_Port: 25565", async function(err) { 
                if(err) throw err; 
                config = await yml("./config/serverstatus.yml");
            })
        }
    } else {
        config = await yml("./config/serverstatus.yml");
    }
    setTimeout(async function() {
        async function update() {
            request(`https://api.minetools.eu/ping/${config.Server_IP}/${config.Server_Port}`, {timeout: 5000 }).then(async res => {
                let category = bot.channels.find(c => c.name.toLowerCase() == config.Server_Status_Category_Name.toLowerCase() && c.type == 'category');
                if(!category) return;
                let guild = category.guild;
                let ip = category.children.find(c => c.name.match(/IP: .+/g));
                let online = category.children.find(c => c.name.match(/Status: (Online|Offline)/g));
                let playerCount = category.children.find(c => c.name.match(/Online: \d+\/\d+/g));
                res = JSON.parse(res);
                let isOnline = res.error || !res.players ? "Offline" : "Online";
                let players = res.error || !res.players ? "0/0" : `${res.players.online}/${res.players.max}`;
                let serverIP = config.Server_Port == 25565 ? config.Server_IP : config.Server_IP + ":" + config.Server_Port;
                if(!online) {
                    let c = await guild.createChannel(`Status: ${isOnline}`, 'voice');
                    await c.setParent(category.id);
                    await c.overwritePermissions(guild.id, {"CONNECT": false, "SPEAK": false});
                } else {
                    await online.setName(`Status: ${isOnline}`);
                }
                if(!playerCount) {
                    let c = await guild.createChannel(`Online: ${players}`, 'voice');
                    await c.setParent(category.id);
                    await c.overwritePermissions(guild.id, {"CONNECT": false, "SPEAK": false});
                } else {
                    await playerCount.setName(`Online: ${players}`);
                } 
                if(!ip) {
                    let c = await guild.createChannel(`IP: ${serverIP}`, 'voice');
                    await c.setParent(category.id);
                    await c.overwritePermissions(guild.id, {"CONNECT": false, "SPEAK": false});
                } else {
                    await ip.setName(`IP: ${serverIP}`);
                } 
            }).catch(async err => {
                let category = bot.channels.find(c => c.name.toLowerCase() == config.Server_Status_Category_Name.toLowerCase() && c.type == 'category');
                if(!category) return;
                let guild = category.guild;
                let ip = category.children.find(c => c.name.match(/IP: .+/g));
                let online = category.children.find(c => c.name.match(/Status: (Online|Offline)/g));
                let playerCount = category.children.find(c => c.name.match(/Online: \d+\/\d+/g));
                let isOnline = "Offline";
                let players = "0/0";
                let serverIP = config.Server_Port == 25565 ? config.Server_IP : config.Server_IP + ":" + config.Server_Port;
                if(!online) {
                    let c = await guild.createChannel(`Status: ${isOnline}`, 'voice');
                    await c.setParent(category.id);
                    await c.overwritePermissions(guild.id, {"CONNECT": false, "SPEAK": false});
                } else {
                    await online.setName(`Status: ${isOnline}`);
                }
                if(!playerCount) {
                    let c = await guild.createChannel(`Online: ${players}`, 'voice');
                    await c.setParent(category.id);
                    await c.overwritePermissions(guild.id, {"CONNECT": false, "SPEAK": false});
                } else {
                    await playerCount.setName(`Online: ${players}`);
                } 
                if(!ip) {
                    let c = await guild.createChannel(`IP: ${serverIP}`, 'voice');
                    await c.setParent(category.id);
                    await c.overwritePermissions(guild.id, {"CONNECT": false, "SPEAK": false});
                } else {
                    await ip.setName(`IP: ${serverIP}`);
                } 
            })
        }
        update();
        setInterval(update, 60000)
    }, 2500)
};